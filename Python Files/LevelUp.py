"""
Auteur: StanSER

Dit script is om de input in standaard vorm (zie onderaan) te krijgen.
"""

### GEEF HET PATH NAAR DE EXCEL FILE ###
FILENAME = "LevelUpTEST.xlsx"
########################################

####### VERANDER NIETS AAN ONDERSTAANDE CODE ########
import pandas as pd
from LevelUp_Calc import level_up_calc

def import_to_string(filename):
    ## Opent de excel sheet en zet deze om naar een lijst met alle wedstrijden (nog niet verwerkt)
    # @param filename: directory naar het bestand
    # @return lijst met alle wedstrijden (niet verwerkt)

    file = pd.ExcelFile(filename)
    sheet = pd.read_excel(file, na_filter=None)
    sheet.drop(sheet.columns[sheet.columns.str.contains('unnamed', case=False)], axis=1, inplace=True)  # Verwijder 'unnamed' kolommen
    data_raw = sheet.to_string()    # Omzetten naar string formaat
    return data_raw.split("\n")     # omzetten naar lijst met splitsing om de newline

def strip_spaces(string):
    ## Verwijder onnodige spaties uit een string
    # @param string waarvan spaties verwijderd mogen worden
    # @return een spatieloze string
    # Onnodige spaties op index 0 of -1

    begin_index = 0
    end_index = len(string) - 1

    if string[begin_index] == " ":
        begin_index += 1
    if string[end_index] == " ":
        end_index -= 1
    return string[begin_index: end_index + 1]

def sorted_file(lijst_raw):
    ## Verwijdert onnodige spaties en maakt lijst bruikbaar (elke info van wedstrijd op eigen plaats in lijst)
    # @param lijst_raw: onverwerkte lijst met enkel wedstrijden
    # @return verwerkte lijst

    gesorteerde_atleten_lijst = []

    for competition in lijst_raw:
        splitted_item = competition.split("  ")
        # print(splitted_item)
        temp_list = []

        i = 0
        while i < len(splitted_item):
            item = splitted_item[i]
            if item != "":
                item = strip_spaces(item)
                temp_list.append(item)
            i += 1

        # print(temp_list)
        gesorteerde_atleten_lijst.append(temp_list)
    # print(gesorteerde_atleten_lijst)
    return gesorteerde_atleten_lijst

def filter_and_format_athletes(atleten_lijst):
    ## Filter op categorie, vanaf cadet tot master (exclusief) en zet om naar standaard formaat
    # @param atleten_lijst: te filteren lijst
    # @return lijst met enkel geldige categorieen in standaard formaat

    atleten_dict = {}
    LIJST_GELDIGE_CATGORIEEN = ["C", "S", "J"]  # Afhankelijk van string in file, "S" voor senior en scholier
    INDEX_NAAM = 1
    INDEX_VOORNAAM = 2
    INDEX_CATEGORIE_EN_DATUM = 3
    SUBINDEX_CATEGORIE_EN_GESCLACHT = 0
    SUBSUBINDEX_CATEGORIE = 0

    for atleet_obj in atleten_lijst:
        # Plaats van gegevens van atleet kan verschillen per file!
        naam = atleet_obj[INDEX_NAAM]
        voornaam = atleet_obj[INDEX_VOORNAAM]
        assert type(naam) == str
        assert type(voornaam) == str
        naam_voornaam = naam + "_" + voornaam
        if naam_voornaam in atleten_dict:
            wedstijd_obj = make_competition(atleet_obj)
            atleten_dict[naam_voornaam]["wedstrijd_lijst"].append(wedstijd_obj)
        else:
            # Selecteer categorie
            categorie_en_datum = atleet_obj[INDEX_CATEGORIE_EN_DATUM]
            splitted_cat_en_dat = categorie_en_datum.split(" ")
            categorie_en_geslacht = splitted_cat_en_dat[SUBINDEX_CATEGORIE_EN_GESCLACHT]
            categorie = categorie_en_geslacht[SUBSUBINDEX_CATEGORIE]
            assert type(categorie) == str
            if categorie in LIJST_GELDIGE_CATGORIEEN:
                # Voeg atleet toe aan atleten_dict
                geslacht = categorie_en_geslacht[1]
                assert type(geslacht) == str
                if geslacht == "M":
                    geslacht = "Vrouw"
                    if categorie == "S":
                        categorie = "Scholier"
                if geslacht == "D":
                    geslacht = "Vrouw"
                    if categorie == "S":
                        categorie = "Senior"

                if geslacht == "J":
                    geslacht = "Man"
                    if categorie == "S":
                        categorie = "Scholier"

                if geslacht == "H":
                    geslacht = "Man"
                    if categorie == "S":
                        categorie = "Senior"

                if categorie == "C":
                    categorie = "Cadet"
                if categorie == "J":
                    categorie = "Junior"

                # helpen_OK =

                temp_dict = {"naam": naam,
                             "voornaam": voornaam,
                             "categorie": categorie,
                             "geslacht": geslacht,
                             "wedstrijd_lijst": []}
                atleten_dict[naam_voornaam] = temp_dict
                wedstijd_obj = make_competition(atleet_obj)
                atleten_dict[naam_voornaam]["wedstrijd_lijst"].append(wedstijd_obj)
    return atleten_dict


def make_competition(atleet_obj):
    ## Krijgt een alteet_obj binnen en zet om naar wedstrijd_obj
    # @param atleet_obj: lijst met gegevens van atleet en wedstrijd
    # @return: wedstrijd_obj

    INDEX_CATEGORIE_EN_DATUM = 3
    SUB_INDEX_DATUM = 1
    INDEX_PLAATS = 4
    INDEX_WEDSTRIJD = 5
    INDEX_RESULTAAT = 6


    categorie_en_datum = atleet_obj[INDEX_CATEGORIE_EN_DATUM].split(" ")
    datum = categorie_en_datum[SUB_INDEX_DATUM]
    datum = normalize(datum)
    plaats = atleet_obj[INDEX_PLAATS]
    wedstrijd = atleet_obj[INDEX_WEDSTRIJD]
    resulultaat = atleet_obj[INDEX_RESULTAAT]
    assert type(datum) == str
    assert type(plaats) == str
    assert type(wedstrijd) == str
    assert type(resulultaat) == str
    if len(atleet_obj) > 7:
        opmerking = atleet_obj[7]
    else:
        opmerking = "GEEN"
    wedstrijd_obj = [datum, plaats, wedstrijd, resulultaat, opmerking]
    return wedstrijd_obj

def normalize(datum):
    ## Verandert bv. '2019-12-26' naar '26/12/2019'
    # @param datum: de te veranderen datum
    # @return de veranderde datum

    jaar_maand_dag = datum.split("-")
    return jaar_maand_dag[2] + '/' + jaar_maand_dag[1] + '/' + jaar_maand_dag[0]

if __name__ == "__main__":
    atleten_lijst_raw = import_to_string(FILENAME)
    gesorteerde_lijst = sorted_file(atleten_lijst_raw[1:])  # Eerste item is de hoofding, mag weggelaten worden
    atleten_standaard = filter_and_format_athletes(gesorteerde_lijst)   # Hier moet eventueel code vervangen worden per file
    """
    - Standaard formaat voor verdere code:
        Dictionary van dictionaries van atleten met
            Naam (str)
            Voornaam (str)
            Categorie (str)
            Geslacht (str)
            (Helpen_OK (boolean)) -> Zelf nakijken per atleet, staat (nog) niet in file
            Wedstrijdlijst (list)
        
        Wedstrijdlijst: lijst met wedstrijdobjecten met:
            Datum (str)
            Plaats (str)
            Wedstrijd (str)
            Resultaat (str)
            Opmerking (str)
            
            (volgorde belangrijk)
            
        Zo kan de volgende code telkens hergebruikt worden.
        
    STANDAARD FORMAAT:
        atleten_standaard["Servaes_Stan"] = {'naam': 'Servaes', 
                                             'voornaam': 'Stan', 
                                             'categorie': 'Junior', 
                                             'geslacht': 'Man', 
                                             'wedstrijd_lijst': [['05/01/2020', 'Gent', '60 m', '7.32', 'PK'], 
                                                                 ['05/01/2020', 'Gent', '60 m', '7.23', 'PK 1'], 
                                                                 ['05/01/2020', 'Gent', '200 m', '23.29', 'PK 2'], 
                                                                 ['27/01/2020', 'Gent', '60 m', '7.22', 'KVV'], 
                                                                 ['27/01/2020', 'Gent', '200 m', '22.80', 'KVV'], 
                                                                 ['02/02/2020', 'Gent', '4 x 200 m', '1:31.77', 'GEEN'], 
                                                                 ['05/02/2020', 'Gent', '60 m', '7.14', 'GEEN'], 
                                                                 ['05/02/2020', 'Gent', '60 m', '7.09', 'GEEN'], 
                                                                 ['05/02/2020', 'Gent', '200 m', '22.66', 'GEEN']]}
    
    - Verbetering clubrecords en Belgische records zelf na te kijken (kan later geimplementeerd worden).
    - Punten op selectie zelf na te kijken.
    - Punten op best behaalde prestatie zelf na te kijken (kan later geimplementeerd worden).
    - Atleet 2 keer geholpen zelf na te kijken.
    """
    level_up_calc(atleten_standaard)
