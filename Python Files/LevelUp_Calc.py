"""
Auteur: StanSER

Dit script berekent de behaalde punten met input in standaardvorm.

~ P: Punten
~ BP: BonusPunten

                                                P / BP      Hoe herkennen
                                                ------      ------------------
WINTER:                                                     maand < 4 of 11 <= maand
ZOMER:                                                      4 <= maand < 11

SYSTEEM:
    3.1. Deelname aan wedstrijden, veldlopen en provinciale kampioenschappen:
        [PK zonder selectie]
        WINTER:
            - Open meeting (indoor):            3 P         Opmerking == "GEEN"
                + RCT organisatie               3 BP        Plaats == Tienen
            - Veldloop:                         3 P         Wedstrijd == Veldloop
                + 3de                           3 BP        Resultaat == 3/x
                + 2de                           9 BP        Resultaat == 2/x
                + 1ste                          18 BP       Resultaat == 1/x
            - Prov. kamp.:                      3 P         Opmerking == PK
                + 3de                           3 BP        Opmerking == PK3
                + 2de                           9 BP        Opmerking == PK2
                + 1ste                          27 BP       Opmerking == PK1
        ZOMER:
            - Open meeting (piste)              4 P         Opmerking == "GEEN"
                + RCT organisatie               4 BP        Plaats == Tienen
            - Prov. kamp.:                      4 P         Opmerking == PK
                + 3de                           4 BP        Opmerking == PK3
                + 2de                           12 BP       Opmerking == PK2
                + 1ste                          36 BP       Opmerking == PK1
        REGELS:
            - Open meeting = piste- of indoorwedstrijd (of combinatie).
            - Max. op 10 verschillende wedstrijden punten verdienen (max. 40 P), BP niet ingerekend.
            - Punten (P) per wedstrijd, niet per proef.
            - Bonuspunten (BP) per proef.
            - Geen limiet op BP
            - Ook scholenkampioenschappen

    3.2. Vlaamse kampioenschappen:
        KVV met selectie, behalve veldloop en marathon
        WINTER:
            - Indoor: (PER PROEF, slechts 1)
                + Deelname (zonder selectie)    3 P         NVT
                + Deelname (met selectie)       6 P         Opmerking == KVV
                + Top 8                         30 P        Opmerking == KVV_top_8
                + 3de                           45 P        Opmerking == KVV3
                + 2de                           60 P        Opmerking == KVV2
                + 1ste                          75 P        Opmerking == KVV1
            - Veldlopen: (PER PROEF, slechts 1)             Proef == veldloop OF marathon
                + Deelname (zonder selectie)    3 P         Opmerking == KVV
                + Deelname (met selectie)       6 P         NVT
                + Top 25%                       30 P        Via resultaat
                + 3de                           45 P        Opmerking == KVV3 OF via resultaat
                + 2de                           60 P        Opmerking == KVV2 OF via resultaat
                + 1ste                          75 P        Opmerking == KVV1 OF via resultaat
        ZOMER:
            - Outdoor: (PER PROEF, slechts 1)
                + Deelname (zonder selectie)    4 P         Proef == marathon, Opmerking == KVV
                + Deelname (met selectie)       8 P         Opmerking == KVV
                + Top 8                         40 P        Opmerking == KVV_top_8
                + 3de                           60 P        Opmerking == KVV3
                + 2de                           80 P        Opmerking == KVV2
                + 1ste                          100 P       Opmerking == KVV1

        REGELS:
            - Bij veldloop minder dan 32 deelnemers: top 25% wordt top 8
            - Diskwalificatie bij finale is gelijk aan top 8
            - Prestatie moet geleverd worden om beloning te ontvangen
            - Scholenkampioenschappen volgens 3.1

    3.3. Belgische kampioenschappen:
        BK met selectie, behalve veldloop en marathon
        WINTER:
            - Indoor: (PER PROEF, slechts 1)
                + Deelname (zonder selectie)    3 P         NVT
                + Deelname (met selectie)       12 P        Opmerking == BK
                + Top 8                         60 P        Opmerking == BK_top_8
                + 3de                           90 P        Opmerking == BK3
                + 2de                           120 P       Opmerking == BK2
                + 1ste                          150 P       Opmerking == BK1
            - Veldlopen: (PER PROEF, slechts 1)             Proef == veldloop OF marathon
                + Deelname (zonder selectie)    3 P         Opmerking == BK
                + Deelname (met selectie)       12 P        NVT
                + Top 25%                       60 P        Via resultaat
                + 3de                           90 P        Opmerking == BK3 OF via resultaat
                + 2de                           120 P       Opmerking == BK2 OF via resultaat
                + 1ste                          150 P       Opmerking == BK1 OF via resultaat
        ZOMER:
            - outdoor: (PER PROEF, slechts 1)
                + Deelname (zonder selectie)    4 P         Proef == marathon, Opmerking == BK
                + Deelname (met selectie)       16 P        Opmerking == BK
                + Top 8                         80 P        Opmerking == BK_top_8
                + 3de                           120 P       Opmerking == BK3
                + 2de                           160 P       Opmerking == BK2
                + 1ste                          200 P       Opmerking == BK1
        REGELS:
            - Bij veldloop minder dan 32 deelnemers: top 25% wordt top 8
            - Diskwalificatie bij finale is gelijk aan top 8
            - Prestatie moet geleverd worden om beloning te ontvangen
            - Scholenkampioenschappen volgens 3.1

    3.4. Europese en wereldkampioenschappen, Olympische spelen:         (NOG NIET VAN TOEPASSING)
        EUROPEES:
            Indoor: (PER PROEF, slechts 1)
            Outdoor: (PER PROEF, slechts 1)
        WERELD:
            Indoor: (PER PROEF, slechts 1)
            Outdoor en Olymp. sp.: (PER PROEF, slechts 1)

    3.5. Clubrecords en Belgische records:
        Clubrecord:                             60 P        Zelf na te kijken
        Belgisch record (ind)                   800 P       Zelf na te kijken
        Belgisch record (aflossing)             200 P       Zelf na te kijken
        REGELS:
            - Record 1 keer te verbreken op seizoen (per proef)
            - Wordt op einde van seizoen bekeken, laatste atleet die record verbreekt krijgt beloning
            - Verbreking record bij eigen categorie en AC telt voor 2 verbrekingen
            - Verbreking Bel. records die enkel in lijst staan:
                Indoor:     https://www.atletiek.be/competitie/atleten/overzicht-belgische-records-indoor
                Outdoor:    https://www.atletiek.be/competitie/atleten/overzicht-belgische-record-outdoor

    3.6. Provinciale, Vlaamse en Belgische selectie:
        Provinciale selectie:                   8 P         Zelf na te kijken
        Vlaamse selectie:                       40 P        Zelf na te kijken
        Belgische selectie:                     200 P       Zelf na te kijken

    3.7. Prestaties:                                        Zelf na te kijken
        Motivatiepunten aan de hand van gelerverde prestatie, zie bijlagen in Level-Up systeem
        REGELS:
            - Meer punten bij hogere categorie: punten van hogere categorie aan atleet
            - Slechts 1 keer punten ontvangen per proef
            - Punten voor max. 6 verschillende proeven

    3.8. Interclubs:
        Selectie interclub                      40 P        Opmerking == interclub
        REGELS:
            - Aanwezigheid verwacht
            - Per categorie (max 80 P te verdienen)

    3.9. Aanwezigheid op niet-sportieve RCT-events en eigen wedstrijden:
        - Minstens twee keer geholpen op een RCT-event of wedstrijd
        - Minstens twee keer aanwezig op een niet-sportief RCT-event (Uitgezonderd Quiz)
        - Minstens één keer geholpen op een RCT-event of wedstrijd en minstens één keer aanwezig op
          een niet-sportief RCT-even
        ZO NIET: -25% VAN DE PUNTEN
"""
# CONVENTIES
# Nieuwe proeven ook toevoegen bij de functies!
# Wedstrijd
ZOMER = 'zomer'
WINTER = 'winter'

Tienen = "Tienen"
PK = "PK"
PK1 = "PK 1"
PK2 = "PK 2"
PK3 = "PK 3"

KVV = "KVV"
KVV_top_8 = "KVV top 8"
KVV3 = "KVV 3"
KVV2 = "KVV 2"
KVV1 = "KVV 1"

BK = "BK"
BK_top_8 = "BK top 8"
BK3 = "BK 3"
BK2 = "BK 2"
BK1 = "BK 1"

interclub = "BVV"

# Loopnummers
veldloop = "Veldloop"
_60m = "60 m"
_100m = "100 m"
_200m = "200 m"
_300m = "300 m"
_400m = "400 m"
_800m = "800 m"
_1500m = "1500 m"
_3000m = "3000 m"
_5000m = "5000 m"
_10000m = "10000 m"
_60m_h = "60 m horden"
_110m_h = "110 m horden"
_100m_h = "100 m horden"
_400m_h = "400 m horden"
steeple = "Steeple"
marathon = "Marathon"
_4x100m = "4 x 100 m"
_4x200m = "4 x 200 m"
_4x400m = "4 x 400 m"

# Kampnummers
hoogspringen = "Hoog"
verspringen = "Ver"
pollstokspringen = "Polsstok"
hinkstapspringen = "HSS"
kogelstoten = "Kogel"
discuswerpen = "Discus"
speerwerpen = "Speer"
hamerslingeren = "Hamer"
gewichtwerpen = "Gewicht"
_5_kamp = "5-kamp"
_7_kamp = "7-kamp"
_8_kamp = "8-kamp"
_10_kamp = "10-kamp"

loopnummers = [veldloop, _60m, _100m, _200m, _300m, _400m, _800m, _1500m, _3000m, _5000m, _10000m, _60m_h, _110m_h, _100m_h, _400m_h, steeple, marathon, _4x100m, _4x200m, _4x400m]
kampnummers = [hoogspringen, verspringen, pollstokspringen, hinkstapspringen, kogelstoten, discuswerpen, speerwerpen, hamerslingeren, gewichtwerpen, _5_kamp, _7_kamp, _8_kamp, _10_kamp]
meerkampen = [_5_kamp, _7_kamp, _8_kamp, _10_kamp]

# Categorieen
cadet = "Cadet"
scholier = "Scholier"
junior = "Junior"
senior = "Senior"

####### VERANDER NIETS AAN ONDERSTAANDE CODE ########

def level_up_calc(atleten_standaard):
    ## Berekent het aantal punten dat de atleet verdiend, motivatiepunten,... niet inegerekend
    # @param atleten_standaard: dictionary met de gegevens van de atleten in standaard vorm
    # @output: Afhankelijk van functie om berekende resultaten weer te geven: txt file per categorie,...


    for atleet in atleten_standaard:
        # Sorteer de wedstrijden volgens puntjes 3.1, 3.2, 3.3 en 3.8
        wedsrijd_veldloop_pk = []
        vlaams_kamp = []
        belgisch_kamp = []
        interclub_lijst = []

        for wedstrijd in atleten_standaard[atleet]["wedstrijd_lijst"]:
            assert len(wedstrijd) == 5
            proef = wedstrijd[2]
            opmerking = wedstrijd[4]
            try:
                assert proef in loopnummers or proef in kampnummers or proef in meerkampen
            except:
                print("Proef <" + proef + "> bij <" + atleet + "> is niet herkend.")
                print("Voeg deze toe aan LevelUp_Calc.py of wijzig de proef.")
            if opmerking == "GEEN" or opmerking[0:2] == PK:
                wedsrijd_veldloop_pk.append(wedstrijd)
            elif opmerking == interclub:
                interclub_lijst.append(wedstrijd)
            elif opmerking[0:3] == KVV:
                vlaams_kamp.append(wedstrijd)
            elif opmerking[0:2] == BK:
                belgisch_kamp.append(wedstrijd)
            else:
                print("Opmerking <" + wedstrijd[4] + "> bij <" + atleet + "> is niet herkend.")
                print("Verander de opmerking in LevelUp_Calc.py of wijzig ze in de sheet.")

        # Bereken de punten per sectie

        # Voor de volgende regels zijn de return statements in de functie niet nodig
        # want functie past parameter aan
        resultaten_vlaams_kamp = calc_points_vlaams_kampioenschap(vlaams_kamp)
        resultaten_belgisch_kamp = calc_points_belgisch_kampioenschap(belgisch_kamp)
        resultaten_interclub = calc_points_inteclub(interclub_lijst)

        # Hier is return statement wel belangrijk want nieuwe variabele wordt aangemaakt
        winter_en_zomerwedstrijden = calc_points_resultaten_veldloop_pk(wedsrijd_veldloop_pk)
        beste_resultaten_per_proef = find_best_results_per_activity(atleten_standaard[atleet]["wedstrijd_lijst"])
        # Voeg resultaten_interclub toe aan atleten_standaard, bij atleet
        atleten_standaard[atleet]["beste_prestaties"] = beste_resultaten_per_proef
        atleten_standaard[atleet]["wedstrijd_lijst"].append(winter_en_zomerwedstrijden)

    print_results_to_txt_file(atleten_standaard)


def print_results_to_txt_file(atleten_standaard):
    ## Geeft atleten_standaard weer in een duidelijke print-out
    # @param atleten_standaard: weer te geven input (dict)
    # @output: txt file per categorie

    # Maak een txt file per categorie
    resultaten_CAD = open(r"Resultaten_CAD", "w")
    resultaten_SCH = open(r"Resultaten_SCH", "w")
    resultaten_JUN = open(r"Resultaten_JUN", "w")
    resultaten_SEN = open(r"Resultaten_SEN", "w")

    # Plaats cursor in het begin van het bestand
    resultaten_CAD.seek(0)
    resultaten_SCH.seek(0)
    resultaten_JUN.seek(0)
    resultaten_SEN.seek(0)

    # Clear het gehele bestand (als bestand al aanwezig was)
    resultaten_CAD.truncate()
    resultaten_SCH.truncate()
    resultaten_JUN.truncate()
    resultaten_SEN.truncate()

    resultaten_CAD.write("CADETTEN" + '\n' + len("CADETTEN")*'-' + 2*'\n')
    resultaten_SCH.write("SCHOLIEREN" + '\n' + len("SCHOLIEREN")*'-' + 2*'\n')
    resultaten_JUN.write("JUNIOREN" + '\n' + len("JUNIOREN")*'-' + 2*'\n')
    resultaten_SEN.write("SENIOREN" + '\n' + len("SENIOREN")*'-' + 2*'\n')

    # CODE OM TE PRINTEN
    for atleet in sorted(atleten_standaard):
        categorie = atleten_standaard[atleet]["categorie"]
        if categorie == cadet:
            file = resultaten_CAD
        elif categorie == scholier:
            file = resultaten_SCH
        elif categorie == junior:
            file = resultaten_JUN
        elif categorie == senior:
            file = resultaten_SEN
        else:
            Error_at_print_results_to_txt_file_1 = False
            assert Error_at_print_results_to_txt_file_1

        naam = atleten_standaard[atleet]["naam"]
        voornaam = atleten_standaard[atleet]["voornaam"]
        categorie = atleten_standaard[atleet]["categorie"]
        geslacht = atleten_standaard[atleet]["geslacht"]
        wedsrijd_lijst = atleten_standaard[atleet]['wedstrijd_lijst']
        beste_prestaties = atleten_standaard[atleet]["beste_prestaties"]
        bonus_punten = 0

        # Print gegevens van atleet
        file.write(voornaam + ' ' + naam + '\n')
        file.write('    ' + categorie + ', ' + geslacht + '\n')
        file.write('    ' + "Wedstrijden:" + '\n')

        # Print wedstrijden en punten
        for wedstrijd in wedsrijd_lijst:
            if len(wedstrijd) > 3:
                datum = wedstrijd[0]
                plaats = wedstrijd[1]
                proef = wedstrijd[2]
                resultaat = wedstrijd[3]
                opmerking = wedstrijd[4]
                punten = wedstrijd[5]
                file.write('        ' + datum.ljust(15, ' ') + plaats.ljust(20, ' ') + proef.ljust(15, ' ') + resultaat.ljust(12, ' ') + opmerking.ljust(15, ' ') + str(punten).ljust(5, ' ') + '\n')
                bonus_punten += punten
            elif len(wedstrijd) == 3:
                file.write('        ' + "Bonuspunten:" + str(bonus_punten).rjust(5, ' ') + '\n')
                aantel_winterwedstrijden = wedstrijd[0]
                aantal_zomerwedstrijden = wedstrijd[1]
                behaalde_punten = wedstrijd[2]
                file.write('        ' + "WINTER:" + str(aantel_winterwedstrijden).rjust(3, ' ') + '\n' + '        ' + 'ZOMER:' + str(aantal_zomerwedstrijden).rjust(4, ' ') + '\n')
                file.write('        ' + 'Punten:' + str(behaalde_punten).rjust(10, ' ') + '\n')
                file.write('        ' + 17*'-' + '\n')
                file.write('        ' + 'TOTAAL:' + str(bonus_punten + behaalde_punten).rjust(10, ' ') + '\n')
            else:
                Error_at_print_results_to_txt_file_2 = False
                assert Error_at_print_results_to_txt_file_2

        # Print beste prestaties
        file.write('    ' + 'Beste prestaties ' + '(voor clubrecords, Belgische records, motivatiepunten, selectie):'+ '\n')
        for proef in beste_prestaties:
            file.write('        ' + proef.ljust(13) + beste_prestaties[proef].rjust(15, ' ') + ' : ' + '\n')
        file.write('                               ' + 20*'-' + '\n')
        file.write('        ' + 'TOTAAL: '.rjust(31, ' ') + '\n')
        file.write('    ' + 83*'=' + '\n')
        file.write('    ' + 'Helpen : ' + '\n')
        file.write('    ' + 'Totaal : ' + '\n')
        file.write('    ' + 'Level  : ' + '\n')

        file.write(2*'\n')

    resultaten_CAD.close()
    resultaten_SCH.close()
    resultaten_JUN.close()
    resultaten_SEN.close()


def calc_points_resultaten_veldloop_pk(wedstrijd_lijst_gewoon_veldloop_pk):
    ## Berekent punten behaald volgens sectie 3.1
    # @param wedstrijd_lijst_gewoon_veldloop_pk: lijst met wedstrijden die voldoen aan sectie 3.1
    # @return (uitgebreide wedstrijd_lijst_gewoon_veldloop_pk met bonuspunten en op laatste index de punten
    #         en) aantal zomer- en winter wedstrijden

    # Als de input llijst leeg is, return derekt None
    if len(wedstrijd_lijst_gewoon_veldloop_pk) == 0:
        return None

    # Sorteer de deelnamen per wedstrijd: punten worden gegeven per wedstrijd, niet per proef.
    # Proeven behoren tot zelfde wedstrijd als:
    #       Plaats is dezelfde
    #       Datum hetzelfde of verschil 1 dag
    #       Opmerking is dezelfde of eerste 2 letters van opmerking zijn dezelfde

    wedstrijd_lijst = []
    winter_wedstrijd_counter = 0
    zomer_wedstrijd_counter = 0

    for deelname in wedstrijd_lijst_gewoon_veldloop_pk:
        # Opmerking is "GEEN" of PK (niet PK1,..)
        dag_nieuw, maand_nieuw, jaar_nieuw, plaats_nieuw, opmerking_nieuw = get_dag_maand_jaar_plaats_opm(deelname)
        voeg_toe_aan_wedstrijd_lijst = True
        for wedstrijd in wedstrijd_lijst:
            dag, maand, jaar, plaats, opmerking = get_dag_maand_jaar_plaats_opm(wedstrijd[0])
            if plaats_nieuw == plaats and opmerking_nieuw[0:2] == opmerking[0:2] and not meer_dan_dag_verschil(dag_nieuw, maand_nieuw, jaar_nieuw, dag, maand, jaar):
                deelname_met_punten = calculate_bonuspoints_3_1(deelname)
                wedstrijd.append([deelname_met_punten])
                voeg_toe_aan_wedstrijd_lijst = False
        if voeg_toe_aan_wedstrijd_lijst:
            # Tel winter- en zomerwedstrijden
            datum = deelname[0]
            if get_season(datum) == WINTER:
                # Geen opmerking, veldloop en Prov. Kamp.: 3 punten
                winter_wedstrijd_counter += 1
            elif get_season(datum) == ZOMER:
                # Geen opmerking en Prov. kamp.: 4 punten
                zomer_wedstrijd_counter += 1
            # Bereken bonuspunten voor de deelname
            deelname_met_punten = calculate_bonuspoints_3_1(deelname)
            wedstrijd_lijst.append([deelname_met_punten])

    zomer_wed = zomer_wedstrijd_counter
    winter_wed = winter_wedstrijd_counter
    behaalde_punten = 0

    while behaalde_punten <= 40 and zomer_wedstrijd_counter > 0:
        behaalde_punten += 4    # 4 punten voor zomerwedstrijd
        zomer_wedstrijd_counter -= 1

    while behaalde_punten <= 40 and winter_wedstrijd_counter > 0:
        behaalde_punten += 3    # 3 punten voor winterwedstrijd
        winter_wedstrijd_counter -= 1

    assert behaalde_punten <= 40
    wedstrijd_lijst.append(behaalde_punten)
    wedstrijd_lijst_gewoon_veldloop_pk.append([winter_wed, zomer_wed, behaalde_punten])
    return [winter_wed, zomer_wed, behaalde_punten]

def calculate_bonuspoints_3_1(deelname):
    ## Berekent het aantal Bonuspunten verdiend voor een deelname volgens sectie 3.1
    # @param deelname: deelname aan wedstrijd als wedstrijdobject
    # @return: deelname + [behaalde punten]
    # WORDT OOK DEREKT IN deelname VERANDERD! Return moet niet gebruikt worden.

    bonuspunten = 0
    datum = deelname[0]
    if get_season(datum) == WINTER:
        proef = deelname[2]
        resultaat = deelname[3]
        opmerking = deelname[4]
        if opmerking[0:2] == PK:  # Opmerking == PK
            if opmerking == PK3:
                bonuspunten = 3
            elif opmerking == PK2:
                bonuspunten = 9
            elif opmerking == PK1:
                bonuspunten = 27
            else:
                bonuspunten = 0
        elif proef == veldloop:
            ranking = resultaat.split("/")[0]
            if ranking == "3":
                bonuspunten = 3
            elif ranking == "2":
                bonuspunten = 9
            elif ranking == "1":
                bonuspunten = 18
            else:
                bonuspunten = 0
        elif opmerking == "GEEN":  # Geen opmerking
            plaats = deelname[1]
            if plaats == Tienen:    # RCT organisatie: 3 BP
                bonuspunten = 3
            else:
                bonuspunten = 0
        else:
            Error_at_calculate_bonuspoints_3_1_WINTER = False
            assert Error_at_calculate_bonuspoints_3_1_WINTER
    elif get_season(datum) == ZOMER:
        opmerking = deelname[4]
        if opmerking[0:2] == PK:
            if opmerking == PK3:
                bonuspunten = 4
            elif opmerking == PK2:
                bonuspunten = 12
            elif opmerking == PK1:
                bonuspunten = 36
            else:
                bonuspunten = 0
        elif opmerking == "GEEN":
            plaats = deelname[1]
            if plaats == Tienen:    # RCT organisatie: 4 BP
                bonuspunten = 4
            else:
                bonuspunten = 0
        else:
            Error_at_calculate_bonuspoints_3_1_ZOMER = False
            assert Error_at_calculate_bonuspoints_3_1_ZOMER
    else:
        Error_at_calculate_bonuspoints_3_1 = False
        assert Error_at_calculate_bonuspoints_3_1
    deelname.append(bonuspunten)
    assert len(deelname) == 6
    return deelname

def get_season(datum):
    ## Kijkt of een gegeven datum in het zomer of winter seizoen valt
    # @param datum: na te kijken datum
    # @return ZOMER of WINTER

    # Winter seizoen: van 31 oktober tot 1 april
    # Zomer seizoen: van 1 april tot 31 oktober
    dag_maand_jaar = datum.split("/")
    maand = int(dag_maand_jaar[1])
    if 4 <= maand < 11:
        return ZOMER
    else:
        return WINTER

def get_dag_maand_jaar_plaats_opm(deelname):
    ## Krijgt deelname aan wedsrtijd, returnt hieruit dag, maand, jaar, plaats en opmerking
    # @param deelname: een wedstrijd object
    # @return: dag, maand, jaar, plaats, opmerking

    dag_maand_jaar = deelname[0].split("/")
    dag = dag_maand_jaar[0]
    maand = dag_maand_jaar[1]
    jaar = dag_maand_jaar[2]
    plaats = deelname[1]
    opmerking = deelname[4]

    return dag, maand, jaar, plaats, opmerking


def meer_dan_dag_verschil(dag_nieuw, maand_nieuw, jaar_nieuw, dag, maand, jaar):
    ## Kijkt na of de 2 data meer dan een dag verschillen
    # @param dag_nieuw, maand_nieuw, jaar_nieuw: Datum 1
    # @param dag, maand, jaar: Datum 2
    # @return True als data meer dan 1 dag veschillen, anders False

    dagen_per_maand = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if is_schrikkeljaar(jaar_nieuw):
        dagen_per_maand_nieuw = list(dagen_per_maand)
        dagen_per_maand_nieuw[1] = 29
    else:
        dagen_per_maand_nieuw = list(dagen_per_maand)

    if is_schrikkeljaar(jaar):
        dagen_per_maand[1] = 29

    dagen_door_maand_nieuw = 0
    dagen_door_maand = 0
    i = 0
    while i < int(maand_nieuw)-1:
        dagen_door_maand_nieuw += dagen_per_maand_nieuw[i]
        i += 1

    i = 0
    while i < int(maand)-1:
        dagen_door_maand += dagen_per_maand[i]
        i += 1

    dagen_nieuw = int(dag_nieuw) + dagen_door_maand_nieuw + 365 * int(jaar_nieuw)
    dagen = int(dag) + dagen_door_maand + 365 * int(jaar)

    if abs(dagen_nieuw - dagen) > 1:
        return True
    else:
        return False

def is_schrikkeljaar(jaar):
    ## Kijkt na of een gegeven jaar een schrikkeljaar is
    # @param jaar: na te kijken jaar
    # @return True or False

    # Voorwaarden schrikkeljaar:
    # - het getal is deelbaar door 4
    # - het getal is niet deelbaar door 100 (!tenzij het deelbaar is door 400!)
    jaar = int(jaar)
    if 0 == jaar % 4 and jaar % 100 != 0:
        return True
    elif jaar % 400 == 0:
        return True
    else:
        return False


def calc_points_vlaams_kampioenschap(wedstrijd_lijst_vlaams):
    ## Berekent punten behaald door deelname aan Vlaams kampioenschap (3.2)
    # @param wedstrijd_lijst_vlaams: lijst met wedstrijden (proeven) op Vlaams kampioenschap
    # @return uitgebreide wedstrijd_lijst_vlaams met punten op laatste index
    # WORDT OOK DEREKT IN wedstrijd_lijst_vlaams AANGEPAST. Return moet niet gebruikt worden

    if len(wedstrijd_lijst_vlaams) == 0:
        return None

    for deelname in wedstrijd_lijst_vlaams:
        punten = 0
        opmerking = deelname[4]
        assert opmerking[0:3] == KVV
        datum = deelname[0]
        seizoen = get_season(datum)
        proef = deelname[2]
        if seizoen == WINTER:
            if proef == veldloop or proef == marathon:  # Zonder selectie
                resultaat = deelname[3]
                ranking = int(resultaat.split("/")[0])
                totaal = int(resultaat.split("/")[1])
                if opmerking == KVV3 or ranking == 3:
                    punten = 45
                elif opmerking == KVV2 or ranking == 2:
                    punten = 60
                elif opmerking == KVV1 or ranking == 1:
                    punten = 75
                elif totaal < 32:
                    if ranking <= 8:
                        punten = 30
                elif ranking/totaal < 0.25:
                    punten = 30
                else:
                    punten = 3
            else:
                if opmerking == KVV_top_8:
                    punten = 30
                elif opmerking == KVV3:
                    punten = 45
                elif opmerking == KVV2:
                    punten = 60
                elif opmerking == KVV1:
                    punten = 75
                else:
                    punten = 6
        elif seizoen == ZOMER:
            if proef == marathon:
                punten = 4
            elif opmerking == KVV_top_8:
                punten = 40
            elif opmerking == KVV3:
                punten = 60
            elif opmerking == KVV2:
                punten = 80
            elif opmerking == KVV1:
                punten = 100
            else:
                punten = 8
        else:
            Error_at_calc_points_vlaams_kampioenschap = False
            assert Error_at_calc_points_vlaams_kampioenschap
        deelname.append(punten)
        assert len(deelname) == 6
    return wedstrijd_lijst_vlaams



def calc_points_belgisch_kampioenschap(wedstrijd_lijst_belgisch):
    ## Berekent punten behaald door deelname aan Vlaams kampioenschap (3.2)
    # @param wedstrijd_lijst_vlaams: lijst met wedstrijden (proeven) op Vlaams kampioenschap
    # @return uitgebreide wedstrijd_lijst_vlaams met punten op laatste index
    # WORDT OOK DEREKT IN wedstrijd_lijst_belgisch AANGEPAST. Return moet niet gebruikt worden

    if len(wedstrijd_lijst_belgisch) == 0:
        return None

    for deelname in wedstrijd_lijst_belgisch:
        punten = 0
        opmerking = deelname[4]
        assert opmerking[0:2] == BK
        datum = deelname[0]
        seizoen = get_season(datum)
        proef = deelname[2]
        if seizoen == WINTER:
            if proef == veldloop or proef == marathon:  # Zonder selectie
                resultaat = deelname[3]
                ranking = int(resultaat.split("/")[0])
                totaal = int(resultaat.split("/")[1])
                if opmerking == BK3 or ranking == 3:
                    punten = 90
                elif opmerking == BK2 or ranking == 2:
                    punten = 120
                elif opmerking == BK1 or ranking == 1:
                    punten = 150
                elif totaal < 32:
                    if ranking <= 8:
                        punten = 60
                elif ranking/totaal < 0.25:
                    punten = 60
                else:
                    punten = 3
            else:
                if opmerking == BK_top_8:
                    punten = 60
                elif opmerking == BK3:
                    punten = 90
                elif opmerking == BK2:
                    punten = 120
                elif opmerking == BK1:
                    punten = 150
                else:
                    punten = 12
        elif seizoen == ZOMER:
            if proef == marathon:
                punten = 4
            elif opmerking == BK_top_8:
                punten = 80
            elif opmerking == BK3:
                punten = 120
            elif opmerking == BK2:
                punten = 160
            elif opmerking == BK1:
                punten = 200
            else:
                punten = 16
        else:
            Error_at_calc_points_belgisch_kampioenschap = False
            assert Error_at_calc_points_belgisch_kampioenschap
        deelname.append(punten)
        assert len(deelname) == 6
    return wedstrijd_lijst_belgisch
def calc_points_inteclub(wedstrijd_lijst_interclub):
    ## Berekent het aantal behaalde punten voor deelname aan interclub
    # @param wedstrijd_lijst_interclub: deelnames aan interclub
    # @return deelname met op laatste index punten behaald
    # WORDT OOK DEREKT IN wedstrijd_lijst_interclub AANGEPAST. Return moet niet gebruikt worden

    # Punten worden gegeven voor deelname aan interclub per categorie
    # Dus max 2 keer punten te verdienen: 1 voor AC en 1 voor eigen categorie

    punten = 40
    reeds_voorgekomen_data = []
    counter = 0
    for deelname in wedstrijd_lijst_interclub:
        datum = deelname[0]
        opmerking = deelname[4]
        assert opmerking == interclub
        if datum not in reeds_voorgekomen_data:
            reeds_voorgekomen_data.append(datum)
            deelname.append(punten)
            counter += 1
        else:
            deelname.append(0)
        assert len(deelname) == 6
    assert counter <= 2
    return wedstrijd_lijst_interclub


def find_best_results_per_activity(wedstrijd_lijst):
    ## Returnt van een gegeven wedstrijdlijst een dictionary met het best resultaat per proef
    # @param wedstrijd_lijst: lisjt met alle wedstrijden van een atleet
    # @return lijst met punten (op volgorde)

    resultaten_dict = {}

    for wedstrijd in wedstrijd_lijst:
        proef = wedstrijd[2]
        resultaat = wedstrijd[3]
        if proef in resultaten_dict:
            if proef in loopnummers:
                # Resultaat moet zo laag mogelijk zijn (veldloop telt niet mee)
                if proef != veldloop and convert_to_seconds(resultaat) < convert_to_seconds(resultaten_dict[proef]):
                    resultaten_dict[proef] = resultaat

            elif proef in kampnummers:
                # Resultaat moet zo hoog mogelijk zijn
                if convert_to_numbers(resultaat) > convert_to_numbers(resultaten_dict[proef]):
                    resultaten_dict[proef] = resultaat
        else:
            resultaten_dict[proef] = resultaat

    return resultaten_dict

def convert_to_seconds(x):
    ## Zet een [string] die tijdsinterval voorstelt om naar een float
    # @param x: string die tijdsinterval voorstelt
    # @return float voorstelling van tijdsinterval in seconden

    # Ofwel: bv. 7.56
    # Ofwel: bv. 1:32.87
    if ":" in x:
        minuten_seconden = x.split(":")
        minuten = int(minuten_seconden[0])
        seconden = float(minuten_seconden[1])
        return minuten * 60 + seconden
    else:
        return float(x)

def convert_to_numbers(x):
    ## Zet resultaat van kampnummer en meerkamp om naar een getal
    # @param x: string die resultaat weergeeft
    # @return resultaat zonder " m" of " ptn"

    splitted_x = x.split(" ")
    number = splitted_x[0].replace(",", ".")
    return float(number)
