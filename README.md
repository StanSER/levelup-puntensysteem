# LevelUp-PuntenSysteem
## Installatie
Om dit programma te laten uitvoeren moet Python geinstalleerd zijn op je computer.
Type 'Python' in de command prompt, als een error verschijnt moet Python nog geinstalleerd worden. 
Surf hiervoor naar https://www.python.org/ .

LevelUp.py maakt gebruik van de library 'pandas', om deze te installeren type je 'pip install pandas' in de command prompt.

## Gebruik
Om het programma te gebruiken donwload je de 2 bestanden bij 'Python Files'. 
LevelUp.py dient om de input (Excel sheet) om te zetten naar een standaard formaat. 
LevelUp_Calc.py gebruikt dit standaard formaat om de punten te berekenen en de beste prestaties eruit te halen. 
Dit wordt geprint in 4 txt files (1 per categorie).

De 2 bestanden moeten in dezelfde folder zitten.
Om aan te geven welke Excel file verwerkt moet worden, open je LevelUp.py (bv. met Notepad), Hier zie je op regel 8 'FILENAME = ...', schrijf hier het path naar de Excel file tussen aanhalingstekens. 
Het gemakkelijkste is om de Excel file in dezelfde folder te plaatsen en de naam (met extensie .xslx) achter het gelijkheidsteken te typen.
De output (4 txt files) wordt door het programma automatisch in dezelfde folder geplaats.

Om het programma te runnen klik je met de rechtermuisknop op LevelUp.py => 'openen met' => Python.

Je kan ook via de command prompt navigeren naar de folder waar de files gedownload zijn en dan 'LevelUp.py' typen.

## Input
Momenteel wordt bij LevelUp.py een input in de volgende vorm verwacht:

<img src="Voorbeeld excel/Voorbeeld.png"  width="1102" height="562">  

De conventies, bv. hoe '100 m' of 'PK 1' worden geschreven, kunnen in LevelUp_Calc.py worden aangepast.
